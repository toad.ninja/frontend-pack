module.exports = {
    env : {
        node : true,
        es6  : true,
    },
    extends: [
        'eslint:recommended',
        'plugin:vue/essential',
        "plugin:jest/recommended"
    ],
    rules: {
        indent: ['error', 4],
    },
    globals: {
        // e.g. "angular": true
    },
    // plugins: [
    //     "jest",
    // ],
};
