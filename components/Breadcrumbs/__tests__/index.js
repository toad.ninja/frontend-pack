/* eslint-env jest */

import { shallowMount } from '@vue/test-utils'
import vBreadcrumbs from '../Breadcrumbs.vue';

function getItems() {
    let items = [];

    for (let i = 0; i < 5; i++) {
        items.push( {
            text : `${i}`,
            url   : `/${i}/`,
        } );
    }

    return items;
}

function getPropsData() {
    return { items: getItems() };
}

describe( 'vBreadcrumbs', () => {

    it( 'has the expected html structure', () => {
        const wrapper = shallowMount( vBreadcrumbs, { propsData: getPropsData(), } );

        expect( wrapper.element ).toMatchSnapshot();
    } );

    it( 'should render n+1 (!!!) __item\'s by default if n items passed (because home element ENABLED)' , () => {
        const wrapper = shallowMount( vBreadcrumbs, { propsData: getPropsData(), } );

        expect(
            wrapper
                .element
                .querySelectorAll('.v-breadcrumbs__item')
        ).toHaveLength( wrapper.props().items.length + 1 );
    } ) ;

    it( 'should render n (!!!) __item\'s by default if n items passed (because home element DISABLED)' , () => {
        const propsData = getPropsData();
        propsData.showHome = false;

        const wrapper = shallowMount( vBreadcrumbs, { propsData, } );

        expect(
            wrapper
                .element
                .querySelectorAll('.v-breadcrumbs__item')
        ).toHaveLength(wrapper.props().items.length);
    } );

    it( 'should allow put html to home element ( !!! non html special chars )' , () => {
        const propsData = getPropsData();
        const homeText = `<svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg"><circle cx="60" cy="60" r="50"></circle></svg><span>Home text</span>`;
        propsData.homeText = homeText;

        const wrapper = shallowMount( vBreadcrumbs, { propsData, } );

        expect( wrapper.html() ).toContain( homeText );
    } );
} );
